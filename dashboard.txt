<div class="row">
                  <div class="col-4 d-flex flex-column align-items-center event-berlalu">
                    <div class="rows d-flex">
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19 4H18V2H16V4H8V2H6V4H5C3.89 4 3.01 4.9 3.01 6L3 20C3 21.1 3.89 22 5 22H19C20.1 22 21 21.1 21 20V6C21 4.9 20.1 4 19 4ZM19 20H5V10H19V20ZM19 8H5V6H19V8ZM12 13H17V18H12V13Z" fill="#797979"/>
                        </svg>
                      <p>Event Berlalu</p>
                    </div>
                    <div class="row">
                      <h4>15 Event</h4>
                    </div>
                  </div>
                  <div class="col-4 d-flex flex-column align-items-center event-aktif">
                    <div class="rows d-flex">
                      	                        
                      <p>Event Aktif</p>
                    </div>
                    <div class="row">
                      <h4>25 Event</h4>
                    </div>
                  </div>
                  <div class="col-4 d-flex flex-column align-items-center event-tersimpan">
                    <div class="rows d-flex">
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14 2H6C4.9 2 4.01 2.9 4.01 4L4 20C4 21.1 4.89 22 5.99 22H18C19.1 22 20 21.1 20 20V8L14 2ZM6 20V4H13V9H18V20H6Z" fill="#797979"/>
                        </svg>
                                               
                      <p>Event Tersimpan</p>
                    </div>
                    <div class="row">
                      <h4>5 Event</h4>
                    </div>
                  </div>
                </div>

                <div class="row chart">
                  <div class="col-6 d-flex align-items-center justify-content-center chart-container flex-column">
                    <p class="judul-chart">Total Penjualan Tiket</p>
                    <canvas id="totalPenjualanTiket"></canvas>
                  </div>
                  <div class="col-6">
                    <div class="row data-view d-flex align-items-center">
                      <div class="col-2 d-flex align-items-center">
                        <i class="material-icons-outlined">
                          visibility
                          </i>    
                      </div>
                      <div class="col-10">
                        <h4>Profil Dilihat</h4>
                        <h2>1.308</h2>
                      </div>
                    </div>
                    <div class="row data-fav d-flex align-items-center">
                      <div class="col-2 d-flex align-items-center">
                        <i class="material-icons-outlined">
                          favorite
                          </i>
                      </div>
                      <div class="col-10">
                        <h4>Favorit Event</h4>
                        <h2>12</h2>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row linechart-container">
                  <h4 class="judul-chart">Total Penjualan</h4>
                  <canvas id="totalPenjualan"></canvas>
                </div>

                <div class="row row-doughnut">
                  <div class="col-6">
                    <div class="doughnut-container">
                      <h4 class="judul-chart">Event Terlaris</h4>
                      <canvas id="eventTerlaris"></canvas>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="doughnut-container">
                      <h4 class="judul-chart">Kategori Terlaris</h4>
                      <canvas id="kategoriTerlaris"></canvas>
                    </div>  
                  </div>
                </div>